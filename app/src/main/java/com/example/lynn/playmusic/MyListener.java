package com.example.lynn.playmusic;

import android.media.MediaPlayer;
import android.view.View;

import static com.example.lynn.playmusic.MainActivity.*;

/**
 * Created by lynn on 6/26/2015.
 */
public class MyListener implements View.OnClickListener {

    @Override
    public void onClick(View v) {
        if (v == play) {
            String audioClip = music.getSelectedItem().toString();

            player = MediaPlayer.create(v.getContext(), ids.get(audioClip));

            player.start();
        } else if (v == stop)
            if (player != null)
                player.stop();
    }

}
