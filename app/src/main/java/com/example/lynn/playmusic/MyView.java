package com.example.lynn.playmusic;

import android.content.Context;
import android.media.MediaPlayer;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import java.util.Arrays;
import java.util.HashMap;

import static com.example.lynn.playmusic.MainActivity.*;

/**
 * Created by lynn on 6/26/2015.
 */
public class MyView extends RelativeLayout {

    public MyView(Context context) {
        super(context);

        ids = new HashMap<>();

        ids.put("act_naturally.mid",R.raw.act_naturally);
        ids.put("africa.mid",R.raw.africa);
        ids.put("albundy0.mid",R.raw.albundy0);
        ids.put("alf.mid",R.raw.alf);
        ids.put("allofme.mid",R.raw.allofme);
        ids.put("allthroughthenight.mid",R.raw.allthroughthenight);
        ids.put("alone.mid",R.raw.alone);
        ids.put("andy_griffith.mid",R.raw.andy_griffith);
        ids.put("another_brick_in_the_wall.mid",R.raw.another_brick_in_the_wall);
        ids.put("astimegoesby.mid",R.raw.astimegoesby);
        ids.put("at17.mid",R.raw.at17);
        ids.put("bad_moon_rising.mid",R.raw.bad_moon_rising);
        ids.put("ballgame.mid",R.raw.ballgame);
        ids.put("beverly_hills_cop.mid",R.raw.beverly_hills_cop);
        ids.put("bingo.mid",R.raw.bingo);
        ids.put("birthday.mid",R.raw.birthday);
        ids.put("blowinginthewind.mid",R.raw.blowinginthewind);
        ids.put("breakmystride.mid",R.raw.breakmystride);
        ids.put("cahereicome1.mid",R.raw.cahereicome1);
        ids.put("caisson.mid",R.raw.caisson);
        ids.put("calledtosayloveyou_2.mid",R.raw.calledtosayloveyou_2);
        ids.put("charlie_brown.mid",R.raw.charlie_brown);
        ids.put("cheers.mid",R.raw.cheers);
        ids.put("come_on_eileen.mid",R.raw.come_on_eileen);
        ids.put("conga.mid",R.raw.conga);
        ids.put("dallas.mid",R.raw.dallas);
        ids.put("danger_zone.mid",R.raw.danger_zone);
        ids.put("do_you_remember_these.mid",R.raw.do_you_remember_these);
        ids.put("enter.mid",R.raw.enter);
        ids.put("eternalflame.mid",R.raw.eternalflame);
        ids.put("fame.mid",R.raw.fame);
        ids.put("final_jeopardy.mid",R.raw.final_jeopardy);
        ids.put("flowrsgone.mid",R.raw.flowrsgone);
        ids.put("ghostriders.mid",R.raw.ghostriders);
        ids.put("grandoldflag.mid",R.raw.grandoldflag);
        ids.put("happytogether.mid",R.raw.happytogether);
        ids.put("happytrails.mid",R.raw.happytrails);
        ids.put("hawaii_50.mid",R.raw.hawaii_50);
        ids.put("hettywainthropinvestigates.mid",R.raw.hettywainthropinvestigates);
        ids.put("hill_street_blues.mid",R.raw.hill_street_blues);
        ids.put("home.mid",R.raw.home);
        ids.put("im_so_excited.mid",R.raw.im_so_excited);
        ids.put("inspirat.mid",R.raw.inspirat);
        ids.put("itsallcomingbacktomenow.mid",R.raw.itsallcomingbacktomenow);
        ids.put("i_love_lucy.mid",R.raw.i_love_lucy);
        ids.put("jambalaya.mid",R.raw.jambalaya);
        ids.put("jeopardy.mid",R.raw.jeopardy);
        ids.put("keepingupappearances.mid",R.raw.keepingupappearances);
        ids.put("kingroad.mid",R.raw.kingroad);
        ids.put("knots_landing.mid",R.raw.knots_landing);
        ids.put("lean_on_me.mid",R.raw.lean_on_me);
        ids.put("liosleepstonight.mid",R.raw.liosleepstonight);
        ids.put("lone_ranger.mid",R.raw.lone_ranger);
        ids.put("longer.mid",R.raw.longer);
        ids.put("loveandmarriage.mid",R.raw.loveandmarriage);
        ids.put("love_boat.mid",R.raw.love_boat);
        ids.put("lucille.mid",R.raw.lucille);
        ids.put("macarena.mid",R.raw.macarena);
        ids.put("marple.mid",R.raw.marple);
        ids.put("mash.mid",R.raw.mash);
        ids.put("memories.mid",R.raw.memories);
        ids.put("mersey.mid",R.raw.mersey);
        ids.put("mission.mid",R.raw.mission);
        ids.put("mockingbirdsong.mid",R.raw.mockingbirdsong);
        ids.put("moonriver.mid",R.raw.moonriver);
        ids.put("muppets.mid",R.raw.muppets);
        ids.put("nyny.mid",R.raw.nyny);
        ids.put("offwego.mid",R.raw.offwego);
        ids.put("okie_from_muskogee.mid",R.raw.okie_from_muskogee);
        ids.put("oldflame.mid",R.raw.oldflame);
        ids.put("oldmac.mid",R.raw.oldmac);
        ids.put("paperroses.mid",R.raw.paperroses);
        ids.put("perry_mason.mid",R.raw.perry_mason);
        ids.put("pianoman.mid",R.raw.pianoman);
        ids.put("picket_f.mid",R.raw.picket_f);
        ids.put("poirot.mid",R.raw.poirot);
        ids.put("popeye.mid",R.raw.popeye);
        ids.put("puff.mid",R.raw.puff);
        ids.put("quincy.mid",R.raw.quincy);
        ids.put("ragtime.mid",R.raw.ragtime);
        ids.put("rainyday.mid",R.raw.rainyday);
        ids.put("rhythmdancer.mid",R.raw.rhythmdancer);
        ids.put("ring_of_fire.mid",R.raw.ring_of_fire);
        ids.put("ritz.mid",R.raw.ritz);
        ids.put("rockin_robin_rockinro.mid",R.raw.rockin_robin_rockinro);
        ids.put("rocky_theme.mid",R.raw.rocky_theme);
        ids.put("rock_me_amadeus.mid",R.raw.rock_me_amadeus);
        ids.put("rule_britannia.mid",R.raw.rule_britannia);
        ids.put("skip.mid",R.raw.skip);
        ids.put("smlworld.mid",R.raw.smlworld);
        ids.put("starspangledbanner.mid",R.raw.starspangledbanner);
        ids.put("starwar.mid",R.raw.starwar);
        ids.put("star_trek.mid",R.raw.star_trek);
        ids.put("surfin_safari.mid",R.raw.surfin_safari);
        ids.put("survivors_eyeofthetiger.mid",R.raw.survivors_eyeofthetiger);
        ids.put("thisland.mid",R.raw.thisland);
        ids.put("thisoldman2.mid",R.raw.thisoldman2);
        ids.put("thosewerethedays.mid",R.raw.thosewerethedays);
        ids.put("titanic.mid",R.raw.titanic);
        ids.put("wabashcannonballaa.mid",R.raw.wabashcannonballaa);
        ids.put("walkin_the_floor_over_you.mid",R.raw.walkin_the_floor_over_you);
        ids.put("washington_post_march.mid",R.raw.washington_post_march);
        ids.put("whole_new_world.mid",R.raw.whole_new_world);
        ids.put("yankee2.mid",R.raw.yankee2);
        ids.put("yesterday.mid",R.raw.yesterday);
        ids.put("yesterdaywheniwasyoung.mid",R.raw.yesterdaywheniwasyoung);
        ids.put("youme.mid",R.raw.youme);
        ids.put("zuthra.mid",R.raw.zuthra);

        String[] list = {
                "act_naturally.mid",
                "africa.mid",
                "albundy0.mid",
                "alf.mid",
                "allofme.mid",
                "allthroughthenight.mid",
                "alone.mid",
                "andy_griffith.mid",
                "another_brick_in_the_wall.mid",
                "astimegoesby.mid",
                "at17.mid",
                "bad_moon_rising.mid",
                "ballgame.mid",
                "beverly_hills_cop.mid",
                "bingo.mid",
                "birthday.mid",
                "blowinginthewind.mid",
                "breakmystride.mid",
                "cahereicome1.mid",
                "caisson.mid",
                "calledtosayloveyou_2.mid",
                "charlie_brown.mid",
                "cheers.mid",
                "come_on_eileen.mid",
                "conga.mid",
                "dallas.mid",
                "danger_zone.mid",
                "do_you_remember_these.mid",
                "enter.mid",
                "eternalflame.mid",
                "fame.mid",
                "final_jeopardy.mid",
                "flowrsgone.mid",
                "ghostriders.mid",
                "grandoldflag.mid",
                "happytogether.mid",
                "happytrails.mid",
                "hawaii_50.mid",
                "hettywainthropinvestigates.mid",
                "hill_street_blues.mid",
                "home.mid",
                "im_so_excited.mid",
                "inspirat.mid",
                "itsallcomingbacktomenow.mid",
                "i_love_lucy.mid",
                "jambalaya.mid",
                "jeopardy.mid",
                "keepingupappearances.mid",
                "kingroad.mid",
                "knots_landing.mid",
                "lean_on_me.mid",
                "liosleepstonight.mid",
                "lone_ranger.mid",
                "longer.mid",
                "loveandmarriage.mid",
                "love_boat.mid",
                "lucille.mid",
                "macarena.mid",
                "marple.mid",
                "mash.mid",
                "memories.mid",
                "mersey.mid",
                "mission.mid",
                "mockingbirdsong.mid",
                "moonriver.mid",
                "muppets.mid",
                "nyny.mid",
                "offwego.mid",
                "okie_from_muskogee.mid",
                "oldflame.mid",
                "oldmac.mid",
                "paperroses.mid",
                "perry_mason.mid",
                "pianoman.mid",
                "picket_f.mid",
                "poirot.mid",
                "popeye.mid",
                "puff.mid",
                "quincy.mid",
                "ragtime.mid",
                "rainyday.mid",
                "rhythmdancer.mid",
                "ring_of_fire.mid",
                "ritz.mid",
                "rockin_robin_rockinro.mid",
                "rocky_theme.mid",
                "rock_me_amadeus.mid",
                "rule_britannia.mid",
                "skip.mid",
                "smlworld.mid",
                "starspangledbanner.mid",
                "starwar.mid",
                "star_trek.mid",
                "surfin_safari.mid",
                "survivors_eyeofthetiger.mid",
                "thisland.mid",
                "thisoldman2.mid",
                "thosewerethedays.mid",
                "titanic.mid",
                "wabashcannonballaa.mid",
                "walkin_the_floor_over_you.mid",
                "washington_post_march.mid",
                "whole_new_world.mid",
                "yankee2.mid",
                "yesterday.mid",
                "yesterdaywheniwasyoung.mid",
                "youme.mid",
                "zuthra.mid"};

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context,android.R.layout.simple_spinner_item, Arrays.asList(list));

        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        music = new Spinner(context);

        music.setAdapter(arrayAdapter);

        play = new Button(context);

        play.setText("Play");

        play.setOnClickListener(listener);

        play.setId(R.id.play);

        stop = new Button(context);

        stop.setText("Stop");

        stop.setOnClickListener(listener);

        stop.setId(R.id.stop);

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);

        layoutParams.addRule(RelativeLayout.RIGHT_OF,R.id.play);

        stop.setLayoutParams(layoutParams);

        layoutParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);

        layoutParams.addRule(RelativeLayout.RIGHT_OF,R.id.stop);

        music.setLayoutParams(layoutParams);

        addView(play);
        addView(stop);
        addView(music);
    }

}
